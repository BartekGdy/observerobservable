package news;

import java.util.Scanner;

/**
 * Created by RENT on 2017-09-18.
 */
public class Main {
    public static void main(String[] args) {
        NewsStation newsStation = new NewsStation();
        newsStation.addWatcher(new Watcher(3));
        newsStation.addWatcher(new Watcher(4));
        newsStation.addWatcher(new Watcher(6));
        newsStation.addWatcher(new Watcher(8));
        newsStation.addWatcher(new Watcher(1));

        newsStation.incommingNews(new News("WTC płonie!!", 10));
        newsStation.incommingNews(new News("Urodziła się świnia", 1));
        newsStation.incommingNews(new News("Promocja w TESCO", 7));

    }
}
