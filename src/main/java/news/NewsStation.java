package news;

import java.util.*;

/**
 * Created by RENT on 2017-09-18.
 */
public class NewsStation extends Observable {
    private Map<String, Integer> news;

    public NewsStation() {
        this.news = new HashMap<>();
    }

    public void incommingNews(News news) {
        if (news!= null) {
            setChanged();
            notifyObservers(news);
        }
    }

    public void addWatcher(Watcher watcher) {
        if (watcher != null) {
            addObserver(watcher);
        }
    }
}
