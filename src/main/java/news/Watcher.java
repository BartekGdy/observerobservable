package news;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by RENT on 2017-09-18.
 */
public class Watcher implements Observer {
    private int panicTreshhold;
    private static int count = 0;
    private int id;

    public Watcher(int panicTreshhold) {
        this.panicTreshhold = panicTreshhold;
        this.id = ++count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Map.Entry) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) arg;
            if (entry.getValue() > this.panicTreshhold) {
                System.out.println(this.id + " : " + "Ulalala ojojoj " + entry.getKey());
            } else {
                System.out.println(this.id + " : " + entry.getKey());
            }
        }
    }
}
