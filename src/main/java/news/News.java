package news;

/**
 * Created by RENT on 2017-09-18.
 */
public class News {
    String newInformation;
    Integer panicLevel;

    public News(String newInformation, Integer panicLevel) {
        this.newInformation = newInformation;
        this.panicLevel = panicLevel;
    }

    public String getNewInformation() {
        return newInformation;
    }

    public void setNewInformation(String newInformation) {
        this.newInformation = newInformation;
    }

    public Integer getPanicLevel() {
        return panicLevel;
    }

    public void setPanicLevel(Integer panicLevel) {
        this.panicLevel = panicLevel;
    }
}
